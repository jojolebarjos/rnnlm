# -*- coding: utf-8 -*-


import io
import itertools
import numpy

from tqdm import tqdm

import torch
import torch.nn as nn
import torch.nn.functional as F


# Multi-head cyclic binary stream
class Stream:
    def __init__(self, path, num_heads):
        self.path = path
        self.num_heads = num_heads
        assert num_heads > 0
        self.length = None
        self.files = []
    
    # Open files when entering context
    def __enter__ (self):
        
        # Open first head and estimate length
        file = io.open(self.path, 'rb')
        self.length = file.seek(0, io.SEEK_END)
        assert self.length > 0
        
        # Open remaining files
        self.files = [file]
        for head in range(1, self.num_heads):
            file = io.open(self.path, 'rb')
            self.files.append(file)
        
        # Properly distribute heads across file
        for head in range(self.num_heads):
            offset = (head * self.length) // self.num_heads
            self.files[head].seek(offset)
        
        return self
    
    # Close files on exit
    def __exit__ (self, type, value, tb):
        for file in self.files:
            file.close()
        self.files = []
    
    # Read chunks from heads
    def read(self, length):
        buffer = numpy.empty((len(self.files), length), dtype=numpy.uint8)
        for head, file in enumerate(self.files):
            chunk = b''
            while True:
                chunk += file.read(length - len(chunk))
                if len(chunk) >= length:
                    break
                file.seek(0)
            buffer[head] = numpy.frombuffer(chunk, dtype=numpy.uint8)
        return torch.from_numpy(buffer)


# Basic RNN-based language model
class BinaryLanguageModel(nn.Module):
    def __init__(self, embedding_size, state_size):
        super().__init__()
        
        # Embed bytes
        self.embedding = nn.Embedding(
            num_embeddings = 256,
            embedding_dim = embedding_size
        )

        # Encode values using GRU
        self.gru = nn.GRU(
            input_size = embedding_size,
            hidden_size = state_size,
            num_layers = 1,
            batch_first = True
        )
        
        # Predict next byte using linear layer
        self.classifier = nn.Linear(
            in_features = state_size,
            out_features = 256
        )
        
        # Compute softmax and cross entropy
        self.loss = nn.CrossEntropyLoss()
    
    # Apply transducer
    def transduce(self, input, previous_state=None):
        
        # Embed bytes
        embedded_input = self.embedding(input.to(torch.long))
        
        # Apply GRU
        encoded_input, state = self.gru(embedded_input, previous_state)
        
        # Predict output
        logits = self.classifier(encoded_input)
        
        # Return outputs
        return logits, state
    
    # Compute loss between input and output, used for training
    def forward(self, input, output, previous_state=None):
        
        # Apply transducer
        logits, state = self.transduce(input, previous_state)
        
        # Compute cross entropy loss
        flat_logits = logits.view(-1, 256)
        flat_output = output.view(-1)
        loss = self.loss(flat_logits, flat_output.to(torch.long))
        return loss, state
    
    # Define binary stream based on learned representation
    def generate(self, anchor=b'', temperature=1):
        # TODO define temperature somewhere
        
        # Feed anchor
        input = torch.from_numpy(numpy.frombuffer(anchor, dtype=numpy.uint8)).view(1, -1)
        logits, state = self.transduce(input)
        
        # Yield infinite stream of predictions
        while True:
            
            # Extract prediction
            scores = logits[0, -1].detach()
            probability = F.softmax(scores / temperature, dim=0).cpu().numpy()
            index = numpy.random.choice(256, p=probability)
            yield bytes([index])
            
            # Compute next step
            input = torch.tensor(index, dtype=torch.long).view(1, 1)
            logits, state = self.transduce(input, state)


# Train language model
def train(
          text_input_path,
          model_output_path,
          batch_size = 64,
          batch_length = 64,
          batch_warmup = 16,
          num_batches_per_epoch = 100,
          num_epochs = None,
          test_anchors = None,
          test_anchor_length = 64,
          # TODO add device
         ):
    
    # Create language model
    model = BinaryLanguageModel(
        embedding_size = 32,
        state_size = 128
    )
    
    # Create optimizer
    # TODO probably use vanilla SGD instead
    # TODO will probably need gradient clipping
    optimizer = torch.optim.Adam(
        params = model.parameters(),
        lr = 0.01
    )
    
    # TODO restore previous model, if any
    
    # Allow interruption at any time
    try:
    
        # Open input file for infinite streaming
        with Stream(text_input_path, batch_size) as stream:
            
            # Initialize state
            previous_batch_state = None
            previous_batch_slice = stream.read(1)
            mean_loss = None
            
            # For each epoch (or infinite, if none)
            epoch = 0
            while num_epochs is None or epoch < num_epochs:
                epoch += 1
            
                # Go through a single epoch
                with tqdm(range(num_batches_per_epoch)) as progress:
                    for step in progress:
                        
                        # Acquire batch
                        batch_output = stream.read(batch_length)
                        batch_input = torch.cat([previous_batch_slice, batch_output[:, :-1]], dim=1)
                        
                        # Apply train step
                        # TODO use warmup at some point
                        batch_loss, batch_state = model(batch_input, batch_output)
                        
                        # Backpropagate gradient
                        optimizer.zero_grad()
                        batch_loss.backward()
                        optimizer.step()
                        
                        # Update progress with moving average
                        batch_loss = float(batch_loss.detach())
                        # TODO also compute sliding std
                        if mean_loss is None:
                            mean_loss = batch_loss
                        else:
                            mean_loss = 0.95 * mean_loss + 0.05 * batch_loss
                        progress.set_description(f'{epoch}) {mean_loss:0.04f}')
                        
                        # Keep state for next step
                        # TODO clarify how to properly reset at random location... maybe only at some symbol (e.g. line break)?
                        #previous_batch_state = batch_state.detach()
                        previous_batch_state = None
                        previous_batch_slice = batch_output[:, -1:]
                
                # Save model
                torch.save(model.state_dict(), model_output_path.format(epoch=epoch))
                
                # If test strings are provided, show inference
                if test_anchors is not None:
                    for test_anchor in test_anchors:
                        output = b''.join(itertools.islice(model.generate(test_anchor), test_anchor_length))
                        print(repr(test_anchor), '->', repr(output))
    
    # Silently discard interruption
    except KeyboardInterrupt:
        pass
