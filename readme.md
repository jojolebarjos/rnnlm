
# RNN-based Character-level Language Model

...

```python
from train import *

train(
    text_input_path = 'input.txt',
    model_output_path = 'model.pt',
    num_batches_per_epoch = 30,
    test_anchors = [
        b'complete this: '
    ]
)
```
